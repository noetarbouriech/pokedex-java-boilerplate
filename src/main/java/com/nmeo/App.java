package com.nmeo;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.dto.SearchPokemonResponse;
import com.nmeo.models.*;
import io.javalin.Javalin;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        Pokedex pkdx = new Pokedex(new HashSet<>());

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
            logger.info("Creating pokemon: " + pokemon.pokemonName);

            // check if request is complete
            if (pokemon.pokemonName == null || pokemon.type == null || pokemon.lifePoints == 0 || pokemon.powers == null) {
                ctx.status(400);
                logger.error("Pokemon creation request is incomplete");
                return;
            }

            if (pkdx.exists(pokemon.pokemonName)) {
                ctx.status(400);
                logger.error("Pokemon already exists");
                return;
            }

            // add pokemon to pokedex
            pkdx.create(pokemon);
            ctx.result("Pokemon created");
        })
        .get("/api/searchByName", ctx -> {
            String nameToSearch = ctx.queryParam("name");
            logger.info("Searching pokemon by name: " + nameToSearch);

            // check if request is complete
            if (nameToSearch == null) {
                ctx.status(400);
                logger.error("Pokemon search request is incomplete");
                return;
            }

            ctx.json(new SearchPokemonResponse(pkdx.get(nameToSearch)));
        })
        .get("/api/searchByType", ctx -> {
            String typeToSearch = ctx.queryParam("type");
            logger.info("Searching pokemon by type: " + typeToSearch);

            // check if request is complete
            if (typeToSearch == null) {
                ctx.status(400);
                logger.error("Pokemon search request is incomplete");
                return;
            }

            // check if type is valid
            try {
                PokemonType.valueOf(typeToSearch);
            } catch (IllegalArgumentException e) {
                ctx.status(400);
                logger.error("Pokemon type is invalid");
                return;
            }

            ctx.json(new SearchPokemonResponse(pkdx.get(PokemonType.valueOf(typeToSearch))));
        })
        .post("/api/modify", ctx -> {
            ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);

            // returns in pokemon is not in pokedex
            if (!pkdx.exists(request.pokemonName)) {
                ctx.status(404);
                logger.error("Pokemon does not exist");
                return;
            }

            // modify pokemon
            pkdx.update(request.pokemonName, request.type, request.lifePoints, request.powers);
            ctx.result("Pokemon modified");
        })
        .start(port);
    }
}