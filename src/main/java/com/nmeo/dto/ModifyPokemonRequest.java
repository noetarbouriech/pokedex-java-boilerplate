package com.nmeo.dto;

import com.nmeo.models.PokemonType;
import com.nmeo.models.Power;

import java.util.List;

public class ModifyPokemonRequest {

    public String pokemonName;
    public PokemonType type;
    public int lifePoints;
    public List<Power> powers;
}
