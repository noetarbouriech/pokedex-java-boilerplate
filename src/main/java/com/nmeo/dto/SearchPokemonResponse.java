package com.nmeo.dto;

import com.nmeo.models.Pokemon;

import java.util.List;

public class SearchPokemonResponse {

    public List<Pokemon> result;

    public SearchPokemonResponse(List<Pokemon> result) {
        this.result = result;
    }
}
