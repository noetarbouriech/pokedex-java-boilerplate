package com.nmeo.models;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Pokedex {

    private Set<Pokemon> pokemons;

    public Set<Pokemon> getPokemons() {
        return pokemons;
    }

    public Pokedex(Set<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    public void create(Pokemon pokemon) {
        pokemons.add(pokemon);
    }

    public List<Pokemon> get(String pokemonName) {
        return pokemons.stream().filter(p -> p.pokemonName.contains(pokemonName)).collect(Collectors.toList());
    }

    public List<Pokemon> get(PokemonType type) {
        return pokemons.stream().filter(p -> p.type.equals(type)).collect(Collectors.toList());
    }

    public boolean exists(String pokemonName) {
        return pokemons.stream().anyMatch(p -> p.pokemonName.equals(pokemonName));
    }

    public void update(String pokemonName, PokemonType type, int lifePoints, List<Power> powers) {
        for (Pokemon p : pokemons) {
            if (p.pokemonName.equals(pokemonName)) {
                p.type = type != null ? type : p.type;
                p.lifePoints = lifePoints != 0 ? lifePoints : p.lifePoints;
                if (powers == null) break;
                for (Power power : powers) {
                    if (p.powers.stream().anyMatch(pw -> pw.powerName.equals(power.powerName))) {
                        for (Power pw : p.powers) {
                            if (pw.powerName.equals(power.powerName)) {
                                pw.damage = power.damage;
                                pw.damageType = power.damageType;
                            }
                        }
                    } else {
                        p.powers.add(power);
                    }
                }
                break;
            }
        }
    }
}
