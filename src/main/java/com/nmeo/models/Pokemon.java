package com.nmeo.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class Pokemon {

    @JsonProperty("pokemonName")
    public String pokemonName;

    @JsonProperty("type")
    public PokemonType type;

    @JsonProperty("lifePoints")
    public int lifePoints;

    @JsonProperty("powers")
    public List<Power> powers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokemon pokemon = (Pokemon) o;
        return lifePoints == pokemon.lifePoints && Objects.equals(pokemonName, pokemon.pokemonName) && type == pokemon.type && Objects.equals(powers, pokemon.powers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pokemonName, type, lifePoints, powers);
    }

}
