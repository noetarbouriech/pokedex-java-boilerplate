package com.nmeo.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Power {

    @JsonProperty("powerName")
    public String powerName;

    @JsonProperty("damageType")
    public PokemonType damageType;

    @JsonProperty("damage")
    public int damage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Power power = (Power) o;
        return damage == power.damage && Objects.equals(powerName, power.powerName) && damageType == power.damageType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(powerName, damageType, damage);
    }
}
